﻿using Microsoft.Extensions.DependencyInjection;
using Take.Nike.Core;
using Take.Nike.Core.Interfaces;
using Take.Nike.Services;

namespace Take.Nike.Injections
{
    public static class InjectionsServices
    {
        public static IServiceCollection AddInjectionsServices(this IServiceCollection services)
        {
			services.AddSingleton<INikeApi, NikeApi>();
            services.AddSingleton<IOrderService, OrderService>();
            services.AddSingleton<ICarouselService, CarouselService>();
            services.AddSingleton<ISmallTalksService, SmallTalksService>();
            services.AddSingleton<IDeskService, DeskService>();
			return services;
        }
    }
}
