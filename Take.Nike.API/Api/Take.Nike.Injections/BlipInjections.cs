﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Take.Blip.Client;
using Take.Blip.Client.Extensions.Context;
using Take.Blip.Client.Extensions.Resource;
using StructureMap;

namespace Take.Nike.Injections
{
    public static class BlipInjections
    {
        public static IServiceProvider AddBlipClientToContainer(this IServiceCollection services, Container container, IBlipClient blipClient, string botIdentifier)
        {

            container.Configure(config =>
            {
                config.For<IResourceExtension>().Add(new ResourceExtension(blipClient)).Named(botIdentifier);
                config.For<IContextExtension>().Add(new ContextExtension(blipClient)).Named(botIdentifier);
                config.For<IBlipClient>().Add(blipClient).Named(botIdentifier);
                config.Populate(services);
            });
            return container.GetInstance<IServiceProvider>();
        }
    }
}
