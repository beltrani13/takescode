﻿using Microsoft.Extensions.DependencyInjection;
using SmallTalks.Core;
using SmallTalks.Core.Services;
using SmallTalks.Core.Services.Interfaces;

namespace Take.Nike.Injections
{
	public static class InjectionsSmallTalks
	{
		public static IServiceCollection AddInjectionsSmallTalks(this IServiceCollection services)
		{
			services.AddSingleton<ISmallTalksDetector, SmallTalksDetector>();
			services.AddSingleton<IFileService, FileService>();
			services.AddSingleton<IConversionService, ModelConversionService>();
			services.AddSingleton<IDetectorDataProviderService, DetectorDataProviderService>();
			services.AddSingleton<ISourceProviderService, LocalSourceProviderService>();
			services.AddSingleton<IWordDetectorFactory, WordDetectorFactory>();
			services.AddTransient<IWordsDetector, WordsDetectorBase>();
			return services;
		}
	}
}
