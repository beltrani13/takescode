﻿using System.Threading.Tasks;
using Take.Nike.Core.Models;

namespace Take.Nike.Core.Interfaces
{
	public interface IDeskService
	{
		Task<BaseResponse<bool>> CheckBusinessHoursAsync(string botKey);
	}
}
