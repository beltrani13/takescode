﻿using System.Threading.Tasks;
using Take.Nike.Core.Models;
using Take.Nike.Core.Models.BO;

namespace Take.Nike.Core.Interfaces
{
	public interface ISmallTalksService
	{
		Task<BaseResponse<SmallTalksBO>> SmallTalksDetectorAsync(string text);
        BaseResponse<string> CleanInputFromSmalltalks(string input);

    }
}
