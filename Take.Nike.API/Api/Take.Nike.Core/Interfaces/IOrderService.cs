﻿using System.Threading.Tasks;
using Take.Nike.Core.Models;
using Take.Nike.Core.Models.Response;

namespace Take.Nike.Core.Interfaces
{
    public interface IOrderService
    {
        Task<BaseResponse<OrderResponse>> RetriveDataByCpfAsync(string cpf);
        Task<BaseResponse<OrderResponse>> RetriveDataByOrderCodeAsync(string orderCode);
    }
}
