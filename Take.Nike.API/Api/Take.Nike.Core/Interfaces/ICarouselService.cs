﻿using Lime.Protocol;
using System.Collections.Generic;
using System.Threading.Tasks;
using Take.Nike.Core.Models;
using Take.Nike.Core.Models.Response;

namespace Take.Nike.Core.Interfaces
{
    public interface ICarouselService
    {
        Task<BaseResponse<DocumentCollection>> GetOrdersCarouselAsync(string orderParamKey, string orderParamValue, bool statusButtonOnly = false, List<Order> orderList = null);
        Task<BaseResponse<DocumentCollection>> GetProductsCarouselAsync(string orderCode, List<Product> productList = null);
    }
}
