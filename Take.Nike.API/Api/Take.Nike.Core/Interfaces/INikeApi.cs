﻿using System.Threading.Tasks;
using Take.Nike.Core.Models;
using Take.Nike.Core.Models.Response;

namespace Take.Nike.Core.Interfaces
{
	public interface INikeApi
    {
        Task<BaseResponse<NikeOrderResponse>> GetDataByCpfAsync(string cpf);

        Task<BaseResponse<NikeOrderResponse>> GetDataByOrderCodeAsync(string orderCode);
    }
}
