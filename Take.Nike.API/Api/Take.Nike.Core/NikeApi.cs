﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Threading.Tasks;
using Take.Nike.Core.Extensions;
using Take.Nike.Core.Interfaces;
using Take.Nike.Core.Models;
using Take.Nike.Core.Models.Response;
using Take.Nike.Core.Models.Settings;

namespace Take.Nike.Core
{
	public class NikeApi : INikeApi
	{
		private readonly string _baseURl;
		private readonly string _getOrderSourceUrl;
		private const string CPF_PARAM = "cpf";
		private const string ORDER_CODE_PARAM = "cod_pedidov";

		public NikeApi(AppSettings appSettings)
		{
			_baseURl = appSettings.NikeSettings.BaseUrl;
			_getOrderSourceUrl = appSettings.NikeSettings.GetOrderSourceUrl;
		}
        public async Task<BaseResponse<NikeOrderResponse>> GetDataByCpfAsync(string cpf)
		{
			return await GetDataByQueryParamAsync(CPF_PARAM, cpf);
		}

		public async Task<BaseResponse<NikeOrderResponse>> GetDataByOrderCodeAsync(string orderCode)
		{
			return await GetDataByQueryParamAsync(ORDER_CODE_PARAM, orderCode);
		}

		public async Task<BaseResponse<NikeOrderResponse>> GetDataByQueryParamAsync(string param, string value)
		{
			var response = new BaseResponse<NikeOrderResponse>();
			try
			{
				var restClient = new RestClient(_baseURl);
				var restRequest = new RestRequest(_getOrderSourceUrl, Method.GET).AddContentTypeJsonHeader();

				if (param.Equals(CPF_PARAM))
				{
					restClient.AddCpfQueryParameter(value);
				}
				else
				{
					restClient.AddOrderCodeQueryParameter(value);
				}
                
				var restResponse = await restClient.ExecuteTaskAsync<NikeOrderResponse>(restRequest);

				if (restResponse.IsSuccessful)
				{
					response.Content = JsonConvert.DeserializeObject<NikeOrderResponse>(restResponse.Content);
				}
				else
				{
					response.Success = false;
					response.Message = $"Request failed on method {nameof(GetDataByQueryParamAsync)}";
				}
			}
			catch (Exception ex)
			{
				response.Success = false;
				response.Exception = ex;
				response.Message = $"Request failed on method {nameof(GetDataByQueryParamAsync)}";
			}
			return response;
		}

	}
}
