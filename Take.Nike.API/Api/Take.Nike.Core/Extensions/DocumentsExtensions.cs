﻿using Lime.Protocol;
using System;
using System.Collections.Generic;
using System.Text;

namespace Take.Nike.Core.Extensions
{
    public static class DocumentsExtensions
    {
        /// <summary>
        /// Creates a <c>DocumentContainer</c> for the given document
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public static DocumentContainer ToDocumentContainer(this Document document)
        {
            return new DocumentContainer { Value = document };
        }
    }
}

