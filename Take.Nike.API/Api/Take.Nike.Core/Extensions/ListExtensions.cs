﻿using System.Collections.Generic;
using Take.Nike.Core.Models.Response;

namespace Take.Nike.Core.Extensions
{
    public static class ListExtensions
    {
        public static List<Order> ToOrderList(this List<NikeOrder> nikeOrders)
        {
            var orderList = new List<Order>();
            foreach (var item in nikeOrders)
            {
                orderList.Add(item.ToOrder());
            }
            return orderList;
        }

        public static List<Product> ToProductList(this List<NikeProduct> nikeProducts)
        {
            var productList = new List<Product>();
            foreach (var item in nikeProducts)
            {
                productList.Add(item.ToProduct());
            }
            return productList;
        }
    }
}
