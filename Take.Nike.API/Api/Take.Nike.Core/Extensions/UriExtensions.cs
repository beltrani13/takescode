﻿using Lime.Messaging.Contents;
using System;
using System.Collections.Generic;
using System.Text;

namespace Take.Nike.Core.Extensions
{
    public static class UriExtensions
    {
        /// <summary>
        /// Creates a <c>MediaLink</c> object with the given parameters
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="text"></param>
        /// <param name="title"></param>
        /// <param name="aspectRatio"></param>
        /// <returns></returns>
        public static MediaLink ToMediaLink(this Uri uri, string text = "", string title = "", string aspectRatio = "2:1") => new MediaLink
        {
            AspectRatio = aspectRatio,
            Uri = (uri.ToString() == "about:blank") ? null : uri,
            Title = title,
            Text = text
        };

        /// <summary>
        /// Creates a <c>WebLink</c> object with the given parameters
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="title"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static WebLink ToWebLink(this Uri uri, string title = "", WebLinkTarget target = WebLinkTarget.Blank) => new WebLink
        {
            Uri = uri,
            Title = title,
            Target = target
        };
    }
}
