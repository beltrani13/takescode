﻿using Lime.Messaging.Contents;
using Lime.Protocol;
using System;
using System.Globalization;

namespace Take.Nike.Core.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Creats a <c>DocumentContainer</c> which <c>Value</c> is a WebLink with the given <paramref name="uri"/> and the referenced <c>string</c> as the <c>Title</c>
        /// </summary>
        /// <param name="s"></param>
        /// <param name="uri">URI to direct the user on click</param>
        public static DocumentContainer ToWebLinkDocumentContainer(this string s, string uri)
        {
            return s.ToWebLinkDocumentContainer(new Uri(uri));
        }

        /// <summary>
        /// Creats a <c>DocumentContainer</c> which <c>Value</c> is a WebLink with the given <paramref name="uri"/> and the referenced <c>string</c> as the <c>Title</c>
        /// </summary>
        /// <param name="s"></param>
        /// <param name="uri">URI to direct the user on click</param>
        public static DocumentContainer ToWebLinkDocumentContainer(this string s, Uri uri)
        {
            return uri.ToWebLink(title: s).ToDocumentContainer();
        }

        /// <summary>
        /// Creates a <c>DocumentContainer</c> which <c>Value</c> is the given string
        /// </summary>
        /// <param name="s"></param>
        public static DocumentContainer ToPlainTextDocumentContainer(this string s)
        {
            return PlainText.Parse(s).ToDocumentContainer(); ;
        }

        /// <summary>
        /// Check if the given date-string is in a day range interval
        /// </summary>
        /// <param dayRange="s"></param>
        public static bool IsInDaysRange(this string dateInput, int dayRange)
        {
            try
            {
                if (dateInput.IsNullOrEmpty())
                {
                    return false;
                }
                else
                {
                    var currentDateTime = DateTime.UtcNow.AddHours(-3);
                    var deliveryDate = DateTime.ParseExact(dateInput, "dd/MM/yyyy HH:mm:ss", null);

                    return (currentDateTime - deliveryDate).TotalDays <= dayRange;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
