﻿using RestSharp;

namespace Take.Nike.Core.Extensions
{
    public static class RestExtensions
    {

        public static RestRequest AddContentTypeJsonHeader(this RestRequest restRequest)
        {
            return (RestRequest)restRequest.AddHeader("Content-Type", "application/json");
        }

        public static RestClient AddCpfQueryParameter(this RestClient restClient, string cpf) 
        {
            return (RestClient)restClient.AddDefaultQueryParameter("cpf", cpf);
        }

        public static RestClient AddOrderCodeQueryParameter(this RestClient restClient, string orderCode)
        {
            return (RestClient)restClient.AddDefaultQueryParameter("cod_pedidov", orderCode);
        }
    }
}
