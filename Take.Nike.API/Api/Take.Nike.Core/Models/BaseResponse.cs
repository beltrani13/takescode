﻿using System;

namespace Take.Nike.Core.Models
{
	public class BaseResponse<T>
	{
		public BaseResponse()
		{
			Message = string.Empty;
			Success = true;
		}

		public T Content { get; set; }
		public string Message { get; set; }
		public Exception Exception { get; set; }
		public bool Success { get; set; }

	}
}
