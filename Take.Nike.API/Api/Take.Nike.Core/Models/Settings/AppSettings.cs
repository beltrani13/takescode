﻿namespace Take.Nike.Core.Models.Settings
{
    public class AppSettings
    {
        public BotConfigurations BotConfigurations { get; set; }
        public NikeSettings NikeSettings { get; set; }
    }
}
