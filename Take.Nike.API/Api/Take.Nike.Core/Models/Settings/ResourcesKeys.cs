﻿namespace Take.Nike.Core.Models.Settings
{
	public class ResourcesKeys
	{
		public string WeekDayScheduleStartKey { get; set; }
		public string WeekDayScheduleStopKey { get; set; }
		public string WeekDayProconScheduleStartKey { get; set; }
		public string WeekDayProconScheduleStopKey { get; set; }
		public string PartialScheduleStartKey { get; set; }
		public string PartialcheduleStopKey { get; set; }
		public string SaturdayScheduleStartKey { get; set; }
		public string SaturdayScheduleStopKey { get; set; }
		public string DayOffListKey { get; set; }
	}
}
