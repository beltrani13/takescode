﻿using System.Collections.Generic;

namespace Take.Nike.Core.Models.Settings
{
    public class BotConfigurations
    {
        public List<Bot> Bots { get; set; }
		public ResourcesKeys ResourcesKeys { get; set; }

		public BotConfigurations()
        {
            Bots = new List<Bot>();
        }
    }
}
