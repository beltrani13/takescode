﻿namespace Take.Nike.Core.Models.Settings
{
    public class Bot
    {
        public string BotId { get; set; }
        public string BotKey { get; set; }
        public string BotAccessKey { get; set; }
    }
}
