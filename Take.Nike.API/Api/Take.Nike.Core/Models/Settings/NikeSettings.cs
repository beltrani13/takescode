﻿using System.Collections.Generic;

namespace Take.Nike.Core.Models.Settings
{
    /// <summary>
    /// Class to use data from appsettings.json "Settings" field
    /// </summary>
    public class NikeSettings
    {
        /// <summary>
        /// Nike's api base url used to make API calls 
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// Nike's api get order's url used to make API calls 
        /// </summary>
        public string GetOrderSourceUrl { get; set; }


        /// <summary>
        /// Nike's logo url used to populate medialink's
        /// </summary>
        public string NikeLogoUrl { get; set; }

        /// <summary>
        /// Nike's url used to provide order tracking
        /// </summary>
        public string OrderTrackingBaseUrl { get; set; }

        /// <summary>
        /// Number of carousel items
        /// </summary>
        public int CarouselItems { get; set; }
    }
}
