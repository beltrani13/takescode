﻿using System;

namespace Take.Nike.Core.Models.BO
{
	public class ScheduleDateTimeBO
	{
		public DateTime? StartDateTime { get; set; }
		public DateTime? StopDateTime { get; set; }
		public bool NoAttendence { get; set; }
	}
}
