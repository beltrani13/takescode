﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Take.Nike.Core.Extensions;

namespace Take.Nike.Core.Models.Response
{
    public class NikeOrder
    {
        private readonly int RefundableDays = 30;

        [JsonProperty("pedidov")]
        public string OrderNumber { get; set; }

        [JsonProperty("cod_pedidov")]
        public string OrderCode { get; set; }

        [JsonProperty("n_pedido_cliente")]
        public string ClientOrderCode { get; set; }

        [JsonProperty("data_emissao")]
        public string IssueDate { get; set; }

        [JsonProperty("data_entrega_prevista")]
        public string ExpectedDeliveryDate { get; set; }

        [JsonProperty("cliente")]
        public string Client { get; set; }

        [JsonProperty("cpf")]
        public string Cpf { get; set; }

        [JsonProperty("cnpj")]
        public string Cnpj { get; set; }

        [JsonProperty("e_mail")]
        public string Email { get; set; }

        [JsonProperty("status_pedido")]
        public string OrderStatus { get; set; }

        [JsonProperty("nota")]
        public string Rating { get; set; }

        [JsonProperty("data_nota")]
        public string RatingDate { get; set; }

        [JsonProperty("transportadora")]
        public string Transporter { get; set; }

        [JsonProperty("tipo_frete")]
        public string ShippingFareType { get; set; }

        [JsonProperty("numero_objeto")]
        public string ObjectNumber { get; set; }

        [JsonProperty("url_tracking_objeto")]
        public string ObjectTrackingUrl { get; set; }

        [JsonProperty("data_entrega")]
        public string DeliveryDate { get; set; }

        [JsonProperty("descricao_sro")]
        public string SroDescription { get; set; }

        [JsonProperty("endereco_entrega")]
        public string DeliveryAddress { get; set; }

        [JsonProperty("v_frete")]
        public string ShippingFareValue { get; set; }

        [JsonProperty("v_total")]
        public double OrderTotalValue { get; set; }

        [JsonProperty("produtospedido")]
        public List<NikeProduct> NikeProducts { get; set; }

        [JsonProperty("aprovado")]
        public string Approved { get; set; }

        [JsonProperty("cond_pgto")]
        public string PaymentCondition { get; set; }

        [JsonProperty("data_pgto")]
        public string PaymentDate { get; set; }

        public NikeOrder()
        {
            NikeProducts = new List<NikeProduct>();
        }
        public Order ToOrder()
        {
            return new Order()
            {
                OrderCode = OrderCode,
                OrderStatus = OrderStatus,
                PaymentCondition = PaymentCondition,
                isRefundable = DeliveryDate.IsInDaysRange(RefundableDays),
                Products = NikeProducts.ToProductList()
            };
        }
    }
}
