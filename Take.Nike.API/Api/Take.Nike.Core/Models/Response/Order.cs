﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Take.Nike.Core.Models.Response
{
    public class Order
    {
        [JsonProperty("OrderCode")]
        public string OrderCode { get; set; }

        [JsonProperty("OrderStatus")]
        public string OrderStatus { get; set; }

        [JsonProperty("PaymentCondition")]
        public string PaymentCondition { get; set; }

        [JsonProperty("isRefundable")]
        public bool isRefundable { get; set; }

        [JsonProperty("Products")]
        public List<Product> Products { get; set; }

        public Order()
        {
            Products = new List<Product>();
        }
    }
}
