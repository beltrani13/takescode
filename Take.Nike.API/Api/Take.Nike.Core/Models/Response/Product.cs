﻿using Newtonsoft.Json;

namespace Take.Nike.Core.Models.Response
{
    public class Product
    {
        [JsonProperty("ProductCode")]
        public string ProductCode { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Size")]
        public string Size { get; set; }

        [JsonProperty("Quantity")]
        public string Quantity { get; set; }

        [JsonProperty("Price")]
        public string Price { get; set; }

    }
}
