﻿using Newtonsoft.Json;

namespace Take.Nike.Core.Models.Response
{
    public class NikeProduct
    {
        [JsonProperty("cod_produto")]
        public string ProductCode { get; set; }

        [JsonProperty("descricao")]
        public string Description { get; set; }

        [JsonProperty("cor")]
        public string Color { get; set; }

        [JsonProperty("tamanho")]
        public string Size { get; set; }

        [JsonProperty("quantidade")]
        public string Quantity { get; set; }

        [JsonProperty("preco")]
        public string Price { get; set; }

        [JsonProperty("barra")]
        public string BarCode { get; set; }

        [JsonProperty("v_total")]
        public string TotalValue { get; set; }

        public Product ToProduct()
        {
            return new Product()
            {
                ProductCode = ProductCode,
                Description = Description,
                Size = Size,
                Quantity = Quantity,
                Price = Price
            };
        }
    }
}
