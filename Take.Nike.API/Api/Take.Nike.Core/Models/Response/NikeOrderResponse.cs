﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Take.Nike.Core.Extensions;

namespace Take.Nike.Core.Models.Response
{
    public class NikeOrderResponse
	{
		[JsonProperty("odata.metadata")]
		public Uri DataMetadata { get; set; }

		[JsonProperty("odata.count")]
		public long DataCount { get; set; }

		[JsonProperty("value")]
		public List<NikeOrder> NikeOrders { get; set; }

		public NikeOrderResponse()
		{
            NikeOrders = new List<NikeOrder>();
		}
        public OrderResponse ToOrderResponse()
        {
            return new OrderResponse()
            {
                OrderCount = DataCount,
                Orders = NikeOrders.ToOrderList()
            };
        }
    }
}
