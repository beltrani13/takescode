﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Take.Nike.Core.Models.Response
{
    public class OrderResponse
    {
        [JsonProperty("OrderCount")]
        public long OrderCount { get; set; }

        [JsonProperty("Orders")]
        public List<Order> Orders { get; set; }

        public OrderResponse()
        {
            Orders = new List<Order>();
        }
    }
}
