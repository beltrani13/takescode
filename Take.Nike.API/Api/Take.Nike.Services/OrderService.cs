﻿using System.Threading.Tasks;
using Take.Nike.Core.Extensions;
using Take.Nike.Core.Interfaces;
using Take.Nike.Core.Models;
using Take.Nike.Core.Models.Response;

namespace Take.Nike.Services
{
    public class OrderService : IOrderService
	{
		private readonly INikeApi _nikeApi;

        public OrderService(INikeApi nikeApi)
		{
            _nikeApi = nikeApi;

        }

		public async Task<BaseResponse<OrderResponse>> RetriveDataByCpfAsync(string cpf)
		{
            var response = new BaseResponse<OrderResponse>();
            var nikeResponse = await _nikeApi.GetDataByCpfAsync(cpf);
            response.Content = nikeResponse.Content.ToOrderResponse();

            return response;

        }

		public async Task<BaseResponse<OrderResponse>> RetriveDataByOrderCodeAsync(string orderCode)
		{
            var response = new BaseResponse<OrderResponse>();
            var nikeResponse = await _nikeApi.GetDataByOrderCodeAsync(orderCode);
            response.Content = nikeResponse.Content.ToOrderResponse();

            return response;
        }
	}
}
