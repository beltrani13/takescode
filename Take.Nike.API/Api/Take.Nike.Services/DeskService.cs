﻿using Lime.Messaging.Contents;
using StructureMap;
using System;
using System.Threading.Tasks;
using Take.Blip.Client.Extensions.Resource;
using Take.Nike.Core.Interfaces;
using Take.Nike.Core.Models;
using Take.Nike.Core.Models.BO;
using Take.Nike.Core.Models.Settings;

namespace Take.Nike.Services
{
	public class DeskService : IDeskService
	{
		private readonly int UTC_TO_BRASILIA_TIME_FACTOR = -3;
		private readonly string ParseTimeFormat = "HH:mm";
		private readonly string ParseDateFormat = "dd/MM";
		private IResourceExtension _resourceExtension;
		private readonly IContainer _botContainer;
		private readonly ResourcesKeys _resources;

		private enum DayType { WeekDay, Saturday, DayOff, Partial };

		public DeskService(AppSettings appSettings, IContainer container)
		{
			_botContainer = container;
			_resources = appSettings.BotConfigurations.ResourcesKeys;
		}

		public async Task<BaseResponse<bool>> CheckBusinessHoursAsync(string botKey)
		{

			_resourceExtension = _botContainer.GetInstance<IResourceExtension>(botKey);

			var currentDateTime = DateTime.UtcNow.AddHours(UTC_TO_BRASILIA_TIME_FACTOR);
			var response = new BaseResponse<bool>();
			try
			{
				response = await CheckForDeskDisponibilityAsync(currentDateTime);
			}
			catch (Exception ex)
			{
				response.Success = false;
				response.Message = "Error getting resource from blip";
				response.Exception = ex;
			}
			return response;
		}

		private async Task<ScheduleDateTimeBO> VerifyScheduleDateTime(string startDate, string endDate)
		{
			var response = new ScheduleDateTimeBO();
			var start = await _resourceExtension.GetAsync<PlainText>(startDate);
			var stop = await _resourceExtension.GetAsync<PlainText>(endDate);
			if (!string.IsNullOrEmpty(start.Text) && start.Text != "off" && !string.IsNullOrEmpty(start.Text) && stop.Text != "off")
			{
				response.StartDateTime = DateTime.ParseExact(start, ParseTimeFormat, null);
				response.StopDateTime = DateTime.ParseExact(stop, ParseTimeFormat, null);
				response.NoAttendence = false;
			}
			else
			{
				response.NoAttendence = true;
			}


			return response;
		}

		private DayType GetDayType(DateTime currentDateTime)
		{

			if (CheckForDayOff(currentDateTime).Result)
			{
				return DayType.DayOff;
			}

			else if (currentDateTime.DayOfWeek.Equals(DayOfWeek.Saturday))
			{
				return DayType.Saturday;
			}

			else if (CheckForPartialDay(currentDateTime).Result)
			{
				return DayType.Partial;
			}

			return DayType.WeekDay;
		}

		private async Task<bool> CheckForDayOff(DateTime currentDateTime)
		{
			var isDayOff = true;
			var DayOffListResource = await _resourceExtension.GetAsync<PlainText>(_resources.DayOffListKey);
			var DayOffList = DayOffListResource.Text.Split(',');

			if (currentDateTime.DayOfWeek.Equals(DayOfWeek.Sunday))
			{
				return isDayOff;
			}
			foreach (var dayOff in DayOffList)
			{
				var dayOffDateTime = DateTime.ParseExact(dayOff.Trim(), ParseDateFormat, null);

				if (currentDateTime.DayOfYear.Equals(dayOffDateTime.DayOfYear))
				{
					return isDayOff;
				}
			}

			return !isDayOff;
		}

		private async Task<bool> CheckForPartialDay(DateTime currentDateTime)
		{

			var PartialListResource = await _resourceExtension.GetAsync<PlainText>(_resources.DayOffListKey);
			var PartialList = PartialListResource.Text.Split(',');
			var isPartialDay = true;

			foreach (var partialDay in PartialList)
			{
				var partialDateTime = DateTime.ParseExact(partialDay.Trim(), ParseDateFormat, null);

				if (currentDateTime.DayOfYear.Equals(partialDateTime.DayOfYear))
				{
					return isPartialDay;
				}
			}

			return !isPartialDay;
		}

		private async Task<BaseResponse<bool>> CheckForDeskDisponibilityAsync(DateTime currentDateTime)
		{
			var response = new BaseResponse<bool>();
			ScheduleDateTimeBO scheduleDateTime;
			var todayType = GetDayType(currentDateTime);
			if (todayType == DayType.DayOff)
			{
				response.Content = false;
				return response;
			}
			else if (todayType == DayType.Saturday)
			{
				scheduleDateTime = await VerifyScheduleDateTime(_resources.SaturdayScheduleStartKey, _resources.SaturdayScheduleStopKey);
			}
			else if (todayType == DayType.Partial)
			{
				scheduleDateTime = await VerifyScheduleDateTime(_resources.PartialScheduleStartKey, _resources.PartialcheduleStopKey);

			}
			else
			{
				scheduleDateTime = await VerifyScheduleDateTime(_resources.WeekDayScheduleStartKey, _resources.WeekDayScheduleStopKey);
			}

			if (!scheduleDateTime.NoAttendence)
			{
				response.Content = currentDateTime.TimeOfDay >= scheduleDateTime.StartDateTime.Value.TimeOfDay &&
									currentDateTime.TimeOfDay <= scheduleDateTime.StopDateTime.Value.TimeOfDay;
			}
			else
			{
				response.Content = false;
			}

			return response;
		}
	}
}