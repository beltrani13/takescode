﻿using Lime.Messaging.Contents;
using Lime.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Take.Nike.Core.Extensions;
using Take.Nike.Core.Interfaces;
using Take.Nike.Core.Models;
using Take.Nike.Core.Models.Response;
using Take.Nike.Core.Models.Settings;

namespace Take.Nike.Services
{
    public class CarouselService : ICarouselService
    {
        private readonly IOrderService _orderService;
        private readonly NikeSettings _nikeSettings;
        private const string CARD_BUTTON_ORDER_SELECT_TEXT = "Selecionar pedido";
        private const string CARD_BUTTON_STATUS_TEXT = "Ver status";
        private const string CARD_BUTTON_BACK_TO_MENU_TEXT = "Voltar ao menu";

        public CarouselService(IOrderService orderService, AppSettings appSettings)
        {
            _orderService = orderService;
            _nikeSettings = appSettings.NikeSettings;
        }

        public async Task<BaseResponse<DocumentCollection>> GetOrdersCarouselAsync(string orderParamKey, string orderParamValue, bool statusButtonOnly = false, List<Order> orderList = null)
        {
            var response = new BaseResponse<DocumentCollection>();
            try
            {
                if (orderList == null)
                {
                    var orderResponse = new BaseResponse<OrderResponse>();

                    if (orderParamKey.Equals("cpf"))
                    {
                        orderResponse = await _orderService.RetriveDataByCpfAsync(orderParamValue);
                    }
                    else
                    {
                        orderResponse = await _orderService.RetriveDataByOrderCodeAsync(orderParamValue);
                    }

                    orderList = orderResponse.Content.Orders.ToList();
                }
                response.Content = BuildCarouselFromOrderList(orderList, statusButtonOnly);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Exception = ex;
                response.Message = $"Request failed on method {nameof(GetOrdersCarouselAsync)}";
            }

            return response;
        }

        private DocumentCollection BuildCarouselFromOrderList(List<Order> orderList, bool statusButtonOnly)
        {
            var collection = new DocumentCollection();
            var carousel = new List<DocumentSelect>();

            orderList.ForEach(o =>
            {
                var card = CreateOrderCard(orderList.IndexOf(o), o, statusButtonOnly);
                carousel.Add(card);
            });

            collection.Items = carousel.ToArray();
            collection.Total = carousel.Count;
            collection.ItemType = DocumentSelect.MIME_TYPE;

            return collection;
        }

        private DocumentSelect CreateOrderCard(int orderIndex, Order order, bool statusButtonOnly)
        {
            var cardTitle = $"<b>Pedido {orderIndex + 1}</b>";
            var cardText = "";

            foreach (var product in order.Products)
            {
                cardText += ($"- {product.Description}<br>");
            }

            return new DocumentSelect
            {
                Header = new Uri(_nikeSettings.NikeLogoUrl).ToMediaLink(cardText, cardTitle).ToDocumentContainer(),
                Options = GenerateOptions(order, statusButtonOnly)
            };
        }

        private DocumentSelectOption[] GenerateOptions(Order order, bool statusButtonOnly)
        {
            var options = new List<DocumentSelectOption>();

            if (statusButtonOnly)
            {
                options.Add(CreateSeeStatusButton(order, 1));
            }
            else
            {
                options.Add(CreateSelectOrderButton(order));
                options.Add(CreateSeeStatusButton(order, 2));
                options.Add(CreateBackToMenuButton());
            }

            return options.ToArray();
        }


        private DocumentSelectOption CreateSelectOrderButton(Order order)
        {
            return new DocumentSelectOption
            {
                Label = CARD_BUTTON_ORDER_SELECT_TEXT.ToPlainTextDocumentContainer(),
                Value = $"#SelecionarPedido_{order.OrderCode}".ToPlainTextDocumentContainer(),
                Order = 1
            };
        }

        private DocumentSelectOption CreateSeeStatusButton(Order order, int buttonIndex)
        {
            return new DocumentSelectOption
            {
                Label = CARD_BUTTON_STATUS_TEXT.ToWebLinkDocumentContainer(_nikeSettings.OrderTrackingBaseUrl + order.OrderCode),
                Order = buttonIndex
            };
        }

        private DocumentSelectOption CreateBackToMenuButton()
        {
            return new DocumentSelectOption
            {
                Label = CARD_BUTTON_BACK_TO_MENU_TEXT.ToPlainTextDocumentContainer(),
                Value = "#Menu".ToPlainTextDocumentContainer(),
                Order = 3
            };
        }

        public async Task<BaseResponse<DocumentCollection>> GetProductsCarouselAsync(string orderCode, List<Product> productList = null)
        {
            var response = new BaseResponse<DocumentCollection>();
            try
            {
                if (productList == null)
                {
                    var orderResponse = new BaseResponse<OrderResponse>();

                    orderResponse = await _orderService.RetriveDataByOrderCodeAsync(orderCode);

                    productList = orderResponse.Content.Orders.FirstOrDefault().Products;
                }
                response.Content = BuildCarouselFromProductList(productList);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Exception = ex;
                response.Message = $"Request failed on method {nameof(GetOrdersCarouselAsync)}";
            }

            return response;
        }

        private DocumentCollection BuildCarouselFromProductList(List<Product> productList)
        {
            var collection = new DocumentCollection();
            var carousel = new List<DocumentSelect>();

            productList.ForEach(o =>
            {
                var card = CreateProductCard(productList.IndexOf(o), o);
                carousel.Add(card);
            });

            collection.Items = carousel.ToArray();
            collection.Total = carousel.Count;
            collection.ItemType = DocumentSelect.MIME_TYPE;

            return collection;
        }

        private DocumentSelect CreateProductCard(int productIndex, Product product)
        {
            var cardTitle = $"<b>Produto {productIndex + 1}</b>";
            var cardText = ($"{product.Description}<br>" +
                                $"R$ {product.Price}<br>" +
                                $"Tamanho: {product.Size}<br>" +
                                $"Quantidade: {product.Quantity}");


            return new DocumentSelect
            {
                Header = new Uri(_nikeSettings.NikeLogoUrl).ToMediaLink(cardText, cardTitle).ToDocumentContainer()
            };
        }
    }
}