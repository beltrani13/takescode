﻿using Take.Nike.Core.Interfaces;
using SmallTalks.Core;
using Take.Nike.Core.Models.BO;
using Take.Nike.Core.Models;
using System.Threading.Tasks;
using SmallTalks.Core.Models;
using System.Linq;
using System;
using System.Text.RegularExpressions;

namespace Take.Nike.Services
{
    public class SmallTalksService : ISmallTalksService
    {
        private readonly ISmallTalksDetector _smallTalksDetector;
        private readonly Regex _cleanNamePattern = new Regex(@"(?i)\b(oi|ol(á|Á|a)|é|É|eh|e|o|a|meu|nome|cpf|(cadastro\sde\spessoa\sf(í|i)sica)|((n|n(ú|u)mero)(.*(pedido|demanda)))|aqui|me|tudo|todo|bem|chamo|bo(m|a)|(dia|tarde|noite)|eu|sou)\b", RegexOptions.Compiled);
        private readonly Regex _ponctuationPattern = new Regex(@"[.,\/#!$%\^&\*;:{}=\-_`~()]", RegexOptions.Compiled);

        public SmallTalksService(ISmallTalksDetector smallTalksDetector)
        {
            _smallTalksDetector = smallTalksDetector;
        }

        public async Task<BaseResponse<SmallTalksBO>> SmallTalksDetectorAsync(string text)
        {
            var response = new BaseResponse<SmallTalksBO>() { Content = new SmallTalksBO() };
            try
            {
                var result = await _smallTalksDetector.DetectAsyncv2(text, new SmallTalksPreProcessingConfiguration() { InformationLevel = InformationLevel.FULL, ToLower = true, UnicodeNormalization = true });
                if (result.HaveCursedWords)
                {
                    response.Content.Intention = "CursedWords";
                }
                else
                {
                    response.Content.Intention = result.Matches.First().SmallTalk;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
                response.Exception = ex;
            }
            return response;
        }

        public BaseResponse<string> CleanInputFromSmalltalks(string input)
        {
            var response = new BaseResponse<string>();

            try
            {

                var cleanedInput = Regex.Replace(input, _cleanNamePattern.ToString(), "").Trim();
                cleanedInput = Regex.Replace(cleanedInput, _ponctuationPattern.ToString(), "").Trim();
                cleanedInput = cleanedInput.Split(" ").FirstOrDefault() ?? string.Empty;
                response.Content = cleanedInput;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
                response.Exception = ex;

            }

            return response;
        }
    }
}
