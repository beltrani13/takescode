﻿using Lime.Messaging.Resources;
using Lime.Protocol;
using Lime.Protocol.Serialization.Newtonsoft;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Serilog;
using Serilog.Exceptions;
using StructureMap;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.IO;
using System.Text;
using Take.Blip.Client;
using Take.Nike.Core.Models.Settings;
using Take.Nike.Injections;

namespace Take.Nike.Api
{
    public class Startup
    {
        public AppSettings AppSettings { get; set; }
        private ILogger LoggerConfiguration { get; set; }
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            string appsettingsPath;

#if DEBUG
            appsettingsPath = $"{AppDomain.CurrentDomain.BaseDirectory}appsettings.{env.EnvironmentName}.json";
#else
			appsettingsPath = $"{AppDomain.CurrentDomain.BaseDirectory}appsettings.json";
#endif

            var builder = new ConfigurationBuilder()
           .AddJsonFile(appsettingsPath, optional: false, reloadOnChange: true)
           .AddEnvironmentVariables();

            LoggerConfiguration = new LoggerConfiguration()
                     .ReadFrom.Configuration(Configuration)
                     .Enrich.WithMachineName()
                     .Enrich.WithProperty("Application", "Take.Api.Nike")
                     .Enrich.WithExceptionDetails()
                     .CreateLogger();

            AppSettings = JsonConvert.DeserializeObject<AppSettings>(File.ReadAllText(appsettingsPath, Encoding.UTF8));

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);			

            services.AddCors();

            #region Dependency injection

            // Serilog			
            services.AddSingleton(LoggerConfiguration);

            // Insert AppSettings
            services.AddSingleton(AppSettings);


            //Insert Lime Deserializer
            services.AddMvc()
            .AddJsonOptions(o => JsonNetSerializer.Settings.Converters.ForEach(c => o.SerializerSettings.Converters.Add(c)));
            

            // Initialize Blip CLients
            var container = new Container();
            foreach (var config in AppSettings.BotConfigurations.Bots)
            {
                var blipClient = new BlipClientBuilder()
                           .UsingHostName("msging.net")
                           .UsingPort(55321)
                           .UsingAccessKey(config.BotId, config.BotAccessKey)
                           .UsingRoutingRule(RoutingRule.Instance)
                           .UsingInstance(config.BotId)
                           .WithChannelCount(1)
                           .Build();
                services.AddBlipClientToContainer(container, blipClient, config.BotId);
            }
            services.AddSingleton<IContainer>(container);

            // Injections Layer
            services.AddInjectionsServices();
			services.AddInjectionsSmallTalks();

			#endregion

			services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Take Nike",
                    Version = "v1",
                    Description = "Api"
                });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Take Nike v1");
                c.DocumentTitle = "Take Nike Documentation";
                c.DocExpansion(DocExpansion.None);
                c.RoutePrefix = string.Empty;
            });

            app.UseCors(c =>
            {
                c.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseAuthentication();
            app.UseSwagger();
            app.UseHttpsRedirection();
        }
    }
}
