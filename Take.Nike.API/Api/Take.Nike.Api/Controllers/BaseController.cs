﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Take.Nike.Api.Controllers
{
	public class BaseController : Controller
	{
		public string BotKey;
		public string BotUser;
		public readonly string BlipBotHeader = "X-Blip-Bot";
		public readonly string BlipUserHeader = "X-Blip-User";

		public override void OnActionExecuting(ActionExecutingContext context)
		{
			var req = context.HttpContext.Request;
			if (req.Headers.ContainsKey(BlipBotHeader))
			{
				BotKey = req.Headers[BlipBotHeader].ToString().Split("@")[0];
				if (req.Headers.ContainsKey(BlipUserHeader))
				{
					BotUser = req.Headers[BlipUserHeader];
				}
			}
			else
			{
				context.Result = Unauthorized();
			}

			base.OnActionExecuting(context);
		}
	}
}