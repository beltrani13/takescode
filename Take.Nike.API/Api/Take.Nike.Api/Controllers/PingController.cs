﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Take.Nike.Core.Interfaces;

namespace Take.Nike.Api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class PingController : Controller
	{
		private readonly IOrderService _orderService;

		public PingController(IOrderService orderService)
		{
			_orderService = orderService;
		}

		/// <summary>
		/// Check client api is up
		/// </summary>
		/// <returns>
		/// 200 up
		/// 400 down
		/// </returns>
		[HttpGet]
		public async Task<IActionResult> Ping()
		{
			var healthCheck = await _orderService.RetriveDataByCpfAsync("0"); 
			if (healthCheck.Success)
			{
				return Json("Pong =)");
			}
			return BadRequest();
		}
	}
}
