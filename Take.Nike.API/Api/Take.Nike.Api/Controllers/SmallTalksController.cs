﻿using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Threading.Tasks;
using Take.Nike.Core.Interfaces;
using Take.Nike.Core.Models.Request;

namespace Take.Nike.Api.Controllers
{
	[Route("[controller]")]
	public class SmallTalksController : Controller
	{
		private readonly ISmallTalksService _smallTalksService;
		private readonly ILogger _logger;
		public SmallTalksController(ISmallTalksService smallTalksService, ILogger logger)
		{
			_smallTalksService = smallTalksService;
			_logger = logger;
		}

		/// <summary>
		/// try detect intention of phrase
		/// </summary>
		/// <param name="body"></param>
		/// <returns></returns>
		[HttpPost("DetectIntention")]
		public async Task<IActionResult> DetectIntention([FromBody]SmallTalksRequest body)
		{
			var resp = await _smallTalksService.SmallTalksDetectorAsync(body.Text);
			if (resp.Success)
			{
				return Ok(resp.Content);
			}
			else
			{
				_logger.Error(resp.Exception, resp.Message);
				return BadRequest(resp.Message);
			}
		}

        [HttpGet("CleanInput")]
        public IActionResult CleanInputFromSmallTalks(string input)
        {
            var resp = _smallTalksService.CleanInputFromSmalltalks(input);
            if (resp.Success)
            {
                return Ok(resp.Content);
            }
            else
            {
                _logger.Error(resp.Exception, resp.Message);
                return BadRequest(resp.Message);
            }
        }
	}
}
