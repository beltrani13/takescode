﻿using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Threading.Tasks;
using Take.Nike.Core.Interfaces;

namespace Take.Nike.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly ICarouselService _carouselService;
        private readonly ILogger _logger;

		public OrderController(IOrderService orderService, ICarouselService carouselService, ILogger logger)
        {
            _orderService = orderService;
            _carouselService = carouselService;
            _logger = logger;
		}

		/// <summary>
		/// Get order data by cpf number
		/// </summary>
		/// <param name="cpf">customer cpf</param>
		/// <returns>Order with products</returns>
		[HttpGet("cpf")]
        public async Task<IActionResult> GetOrderDataByCpfAsync(string cpf)
        {
           var resp = await _orderService.RetriveDataByCpfAsync(cpf);
			if (resp.Success)
			{
				return Ok(resp.Content);
			}
			else
			{
				_logger.Error(resp.Exception, resp.Message);
				return BadRequest(resp.Message);
			}
		}

		/// <summary>
		/// Get order data by order code
		/// </summary>
		/// <param name="orderCode">order code</param>
		/// <returns>Order with products</returns>
		[HttpGet("code")]
        public async Task<IActionResult> GetOrderDataByCodeAsync(string orderCode)
        {
			var resp = await _orderService.RetriveDataByOrderCodeAsync(orderCode);
			if (resp.Success)
			{
				return Ok(resp.Content);
			}
			else
			{
				_logger.Error(resp.Exception, resp.Message);
				return BadRequest(resp.Message);
			}
		}

        /// <summary>
		/// Get order carousel by cpf or orderCode 
		/// </summary>
		/// <param name="keyType">can be "cpf" or "orderCode"</param>
        /// <param name="value">value</param>
        /// <param statusButtonOnly="value">set the buttons layout type</param>
		/// <returns>Order with products</returns>
		[HttpGet("carousel")]
        public async Task<IActionResult> GetOrderCarouselByCodeAsync(string keyType, string value, bool statusButtonOnly = false)
        {
            var resp = await _carouselService.GetOrdersCarouselAsync(keyType, value, statusButtonOnly);
            if (resp.Success)
            {
                return Ok(resp.Content);
            }
            else
            {
                _logger.Error(resp.Exception, resp.Message);
                return BadRequest(resp.Message);
            }
        }


        /// <summary>
        /// Get products carousel by orderCode 
        /// </summary>
        /// <param name="orderCode"> orderCode </param>
        /// <returns>Order of products</returns>
        [HttpGet("carouselProducts")]
        public async Task<IActionResult> GetProductsCarouselAsync(string orderCode)
        {
            var resp = await _carouselService.GetProductsCarouselAsync(orderCode);
            if (resp.Success)
            {
                return Ok(resp.Content);
            }
            else
            {
                _logger.Error(resp.Exception, resp.Message);
                return BadRequest(resp.Message);
            }
        }
    }
}
