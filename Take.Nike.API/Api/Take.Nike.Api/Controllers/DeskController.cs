﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Threading.Tasks;
using Take.Nike.Core.Interfaces;

namespace Take.Nike.Api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class DeskController : BaseController
	{
		private readonly IDeskService _deskService;
		private readonly ILogger _logger;

		public DeskController(IDeskService deskService, ILogger logger)
		{
			_deskService = deskService;
			_logger = logger;
		}

		/// <summary>
		/// Check Office Hours
		/// </summary>
		/// <returns>true or false.</returns>
		[HttpGet("Attendance")]
		public async Task<ActionResult> GetAttendanceAsync()
		{
			var resp = await _deskService.CheckBusinessHoursAsync(BotKey);
			if (resp.Content)
			{
				return Ok(resp.Content);
			}
			else
			{
				_logger.Error(resp.Exception, resp.Message);
				return StatusCode(StatusCodes.Status503ServiceUnavailable);
			}
		}
	}
}
